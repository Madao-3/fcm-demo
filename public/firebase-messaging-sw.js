importScripts("https://www.gstatic.com/firebasejs/3.6.8/firebase-app.js");
importScripts("https://www.gstatic.com/firebasejs/3.6.8/firebase-messaging.js");

// Initialize Firebase
var config = {
  apiKey: "AIzaSyD3OfYzEFnmNEPJ910BbWJO5RphfUwkGo8",
  // authDomain:        "fcm-demo-821ab.firebaseapp.com",
  // databaseURL:       "https://fcm-demo-821ab.firebaseio.com",
  // storageBucket:     "fcm-demo-821ab.appspot.com",
  messagingSenderId: "12345"
};

firebase.initializeApp(config);

const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(payload => {
  console.log("[worker] Received push notification: ", payload);
  return self.registration.showNotification(payload.title, payload);
});
