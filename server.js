"use strict";

const path = require("path");
const { sleep } = require("sleep");

const config = require("./config");
const Koa = require("koa");
const Router = require("koa-router");
const serve = require("koa-static");
const convert = require("koa-convert");
const FCM = require("fcm-node");

const koa = new Koa();
const app = new Router();

koa.use(convert(serve(path.join(__dirname, "public"))));
koa.use(app.routes());
console.log(`Starting server on port ${config.port}...`);
koa.listen(config.port);
